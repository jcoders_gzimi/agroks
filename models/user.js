const Sequelize = require("sequelize");
const sequelize = require("./../db/db_connection");

const User = sequelize.define("user", {
  name: {
    type: Sequelize.STRING,
  },
  email: {
    type: Sequelize.STRING,
    unique: true,
    allowNull: false,
  },
  phone: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  password: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  img_url: {
    type: Sequelize.STRING,
  },
  language: {
    type: Sequelize.ENUM("AL", "EN", "TR"),
    defaultValue: "AL",
  },
  is_admin: {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
  },
  is_superadmin: {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
  },
  membership: {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
  },
});

module.exports = User;
