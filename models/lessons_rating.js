const Sequelize = require("sequelize");
const sequelize = require("./../db/db_connection");
const Lesson = require("./lesson");
const User = require("./user");

const LessonsRating = sequelize.define("lessons_rating", {
  id:{
    type:Sequelize.INTEGER,
    autoIncrement:true,
    primaryKey:true
  },
    rating:{
        type:Sequelize.INTEGER,
        defaultValue: 0,
    }
});
LessonsRating.belongsTo(Lesson, {
  foreignKey: {
    unique: "rating",
  },
});
Lesson.hasMany(LessonsRating)
LessonsRating.belongsTo(User, {
  foreignKey: {
    unique: "rating",
  },
});
User.hasMany(LessonsRating);
User.belongsToMany(Lesson, { through: LessonsRating,foreignKey:"userId" });
Lesson.belongsToMany(User, { through: LessonsRating,foreignKey:"lessonId" });
module.exports = LessonsRating;
