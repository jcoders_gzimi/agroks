const Sequelize = require('sequelize')
const sequelize = require("./../db/db_connection");

const Ad = sequelize.define("ad", {
  url: {
    type: Sequelize.STRING,
    allowNull:false
  },
});

module.exports = Ad;
