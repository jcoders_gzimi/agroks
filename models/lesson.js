const Sequelize = require("sequelize");
const sequelize = require("./../db/db_connection");
const Category = require('./category')

const Lesson = sequelize.define("lesson", {
  url: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  title: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  description: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  type: {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
  },
  is_free: {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
  },
});
Lesson.belongsTo(Category);
Category.hasMany(Lesson)


module.exports = Lesson;
