const Sequelize = require("sequelize");
const sequelize = require("./../db/db_connection");
const LessonsComment = require("./lessons_comment");
const User = require("./user");

const CommentsLike = sequelize.define("comments_like", {
  id:{
    type:Sequelize.INTEGER,
    autoIncrement:true,
    primaryKey:true
  }
});

CommentsLike.belongsTo(LessonsComment, {
  foreignKey: {
    unique: "like",
  },
});
LessonsComment.hasMany(CommentsLike);


CommentsLike.belongsTo(User, {
  foreignKey: {
    unique: "like",
  },
});
User.hasMany(CommentsLike);




User.belongsToMany(LessonsComment, { through: CommentsLike,foreignKey:"userId" });
LessonsComment.belongsToMany(User, { through: CommentsLike,foreignKey:"lessonCommentId" });

module.exports = CommentsLike;
