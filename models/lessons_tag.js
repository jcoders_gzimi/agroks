const Sequelize = require("sequelize");
const sequelize = require("./../db/db_connection");
const Tag = require("./tag");
const Lesson = require("./lesson");

const LessonTag = sequelize.define("lessons_tag", {
  id:{
    type:Sequelize.INTEGER,
    autoIncrement:true,
    primaryKey:true
  }
});
LessonTag.belongsTo(Tag, {
  
  foreignKey: {
    unique: "lessontag",
  },
});
Tag.hasMany(LessonTag)
LessonTag.belongsTo(Lesson, {
  foreignKey: {
    unique: "lessontag",
  },
});
Lesson.hasMany(LessonTag);
Tag.belongsToMany(Lesson, { through: LessonTag,foreignKey:"tagId" });
Lesson.belongsToMany(Tag, { through: LessonTag,foreignKey:"lessonId" });
module.exports = LessonTag;
