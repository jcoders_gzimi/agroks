const Sequelize = require("sequelize");
const sequelize = require("./../db/db_connection");

const MembershipsCode = sequelize.define("memberships_code", {
  code: {
    type: Sequelize.STRING,
    allowNull:false
  },
  status: {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
  },
});

module.exports = MembershipsCode
