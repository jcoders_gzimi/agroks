const Sequelize = require("sequelize");
const sequelize = require("./../db/db_connection");
const Lesson = require("./lesson");
const User = require("./user");

const LessonsComment = sequelize.define("lessons_comment", {
  id:{
    type:Sequelize.INTEGER,
    autoIncrement:true,
    primaryKey:true
  },
  comment: {
    type: Sequelize.TEXT,
    allowNull:false
  },

});
LessonsComment.belongsTo(Lesson);
Lesson.hasMany(LessonsComment)
LessonsComment.belongsTo(User);
User.hasMany(LessonsComment);

User.belongsToMany(Lesson, { through: LessonsComment,foreignKey:"userId" });
Lesson.belongsToMany(User, { through: LessonsComment,foreignKey:"lessonId" });

module.exports = LessonsComment;
