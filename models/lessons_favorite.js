const Sequelize = require("sequelize");
const sequelize = require("./../db/db_connection");
const Lesson = require("./lesson");
const User = require("./user");

const LessonsFavorite = sequelize.define("lessons_favorite", {
  id:{
    type:Sequelize.INTEGER,
    autoIncrement:true,
    primaryKey:true
  }
});
LessonsFavorite.belongsTo(Lesson,{
    foreignKey:{
        unique: "favorite"
    }
})
Lesson.hasMany(LessonsFavorite);
LessonsFavorite.belongsTo(User, {
  foreignKey: {
    unique: "favorite",
  },
});
User.hasMany(LessonsFavorite, {
  foreignKey: {
    name: "userId",
  },
});
User.belongsToMany(Lesson, { through: LessonsFavorite, foreignKey: 'userId' })
Lesson.belongsToMany(User, { through: LessonsFavorite,foreignKey: 'lessonId' })

module.exports = LessonsFavorite;
