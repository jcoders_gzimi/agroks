const Sequelize = require("sequelize");
const sequelize = require("./../db/db_connection");

const Tag = sequelize.define("tag", {
  name: {
    type: Sequelize.STRING,
    allowNull:false
  },
});

module.exports = Tag;
