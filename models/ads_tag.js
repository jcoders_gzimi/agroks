const Sequelize = require("sequelize");
const sequelize = require("./../db/db_connection");
const Tag = require("./tag");
const Ad = require("./ad");

const AdsTag = sequelize.define("ads_tag", {
  id:{
    type:Sequelize.INTEGER,
    autoIncrement:true,
    primaryKey:true
  }
});
AdsTag.belongsTo(Tag, {
  foreignKey: {
    unique: "adstag",
  },
})
Tag.hasMany(AdsTag)
AdsTag.belongsTo(Ad, {
  foreignKey: {
    unique: "adstag",
  },
});
Ad.hasMany(AdsTag);
Tag.belongsToMany(Ad, { through: AdsTag ,foreignKey: 'tagId'});
Ad.belongsToMany(Tag, { through: AdsTag ,foreignKey: 'adId'});

module.exports = AdsTag;
