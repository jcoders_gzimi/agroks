const Sequelize = require("sequelize");
const sequelize = require("./../db/db_connection");
const MembershipsCode = require("./memberships_code");
const User = require("./user");

const Membership = sequelize.define("membership", {
  start_date: {
    type: Sequelize.DATEONLY,
    allowNull:false
  },
  end_date: {
    type: Sequelize.DATEONLY,
    allowNull:false
  },
});
Membership.belongsTo(User)
User.hasMany(Membership)
Membership.belongsTo(MembershipsCode,{
    foreignKey:{
        name: "codeId",
        unique: true
    }
})
MembershipsCode.hasOne(Membership, {
  foreignKey: {
    name: "codeId",
    unique: true,
  },
});

module.exports = Membership
