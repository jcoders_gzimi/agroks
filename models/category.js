const Sequelize = require('sequelize')
const sequelize = require("./../db/db_connection");

const Category = sequelize.define("category", {
  name: {
    type: Sequelize.STRING,
    allowNull:false
  },
});

module.exports = Category