const modTag = require("../models/tag");
const validators = require("../validators/validators");

async function index(req, res) {
  await modTag
    .findAll()
    .then((items) => {
      res.status(200).json({
        status: 0,
        message: items,
      });
    })
    .catch((err) => {
      res.status(400).json({
        status: 0,
        errors: err,
      });
    });
}

function store(req, res) {
  let validatorRes = validators.tagCreate(req.body);
  if (!validatorRes.is_valid) {
    res.status(422).json({
      status: 0,
      errors: validatorRes.errors,
    });
    return;
  }
  let newTag = {
    name: req.body.name,
  };
  modTag
    .create(newTag)
    .then((tag) => {
      res.status(200).json({
        status: 1,
        message: tag,
      });
    })
    .catch((err) => {
      res.status(400).json({
        status: 0,
        message: err,
      });
    });
}

function show(req, res) {
  const tagId = req.params.id;
  modTag
    .findOne({
      where: {
        id: tagId,
      },
    })
    .then((item) => {
      res.status(200).json({
        status: 1,
        message: item,
      });
    })
    .catch((err) => {
      res.status(400).json({
        status: 0,
        message: err,
      });
    });
}

function update(req, res) {
  const tagId = req.params.id;
  modTag
    .findOne({
      where: {
        id: tagId,
      },
    })
    .then((item) => {
      let currTag = item.dataValues;
      let data = { ...currTag, ...req.body };
      let validatorRes = validators.tagCreate(data);
      if (!validatorRes.is_valid) {
        res.status(422).json({
          status: 0,
          errors: validatorRes.errors,
        });
        return;
      }
      return item.update(data);
    })
    .then((updatedTag) => {
      respond.respondSuccess(res, updatedTag);
    })
    .catch((err) => {
      res.status(400).json({
        status: 0,
        message:
          Object.keys(err).length === 0 ? "This tag does not exist" : err,
      });
    });
}

function destroy(req, res) {
  const tagId = req.params.id;
  modTag
    .findOne({
      where: {
        id: tagId,
      },
    })
    .then((item) => {
      return item.destroy();
    })
    .then(() => {
      res.status(200).json({
        status: 1,
        message: "The tag has been deleted",
      });
    })
    .catch((err) => {
      res.status(400).json({
        status: 0,
        message:
          Object.keys(err).length === 0 ? "This tag does not exist" : err,
      });
    });
}

const tag_crud = {
  index,
  store,
  show,
  update,
  destroy,
};

module.exports = tag_crud;
