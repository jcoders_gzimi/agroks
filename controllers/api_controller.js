function respondSuccess(res, data, status = 200) {
  res.status(status).json({
    status: 1,
    data,
  });
}
function respondNotFound(res, errors = "Invalid input!", status = 404) {
  res.status(status).json({
    status: 0,
    errors,
  });
}
function respondError(res, errors, status = 422) {
  res.status(status).json({
    status: 0,
    errors,
  });
}

const respond = {
  respondSuccess,
  respondError,
  respondNotFound,
};
module.exports = respond;
