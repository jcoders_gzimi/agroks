const modLessonTag = require("../models/lessons_tag");
const validators = require("../validators/validators");
const respond = require("./api_controller");

async function index(req, res) {
  await modLessonTag
    .findAll()
    .then((items) => {
      if (items.length > 0) {
        respond.respondSuccess(res, items);
      } else {
        respond.respondNotFound(res, "Lesson-tag not exists");
      }
    })
    .catch((err) => {
      res.status(500).json({
        status: 0,
        errors: err,
      });
    });
}

function store(req, res) {
  let validatorRes = validators.lessonTagCreate(req.body);
  if (!validatorRes.is_valid) {
    respond.respondError(res, validatorRes.errors);
    return;
  }
  let newLessonTag = {
    tagId: req.body.tagId,
    lessonId: req.body.lessonId,
  };

  modLessonTag
    .create(newLessonTag)
    .then((lessonTag) => {
      if (lessonTag) {
        respond.respondSuccess(res, lessonTag);
      } else {
        respond.respondError(res, "Lesson-tag registration failed");
      }
    })
    .catch((err) => {
      res.status(500).json({
        status: 0,
        message: err,
      });
    });
}

function show(req, res) {
  const lessonTagId = req.params.id;
  modLessonTag
    .findOne({
      where: {
        id: lessonTagId,
      },
    })
    .then((item) => {
      if (item) {
        respond.respondSuccess(res, item);
      } else {
        respond.respondNotFound(res, "This Lesson-tag not exists");
      }
    })
    .catch((err) => {
      res.status(500).json({
        status: 0,
        message: err,
      });
    });
}

function update(req, res) {
  const lessonTagId = req.params.id;
  modLessonTag
    .findOne({
      where: {
        id: lessonTagId,
      },
    })
    .then((item) => {
      return item.update({
        ...req.body,
      });
    })
    .then(() => {
      respond.respondSuccess(res, "The Lesson-tag has been updated");
    })
    .catch((err) => {
      res.status(400).json({
        status: 0,
        message:
          Object.keys(err).length === 0
            ? "This lesson tag does not exist"
            : err,
      });
    });
}

function destroy(req, res) {
  const lessonTagId = req.params.id;
  modLessonTag
    .findOne({
      where: {
        id: lessonTagId,
      },
    })
    .then((item) => {
      if (item) {
        item
          .destroy()
          .then(respond.respondSuccess(res, "Lesson-tag has been deleted"));
      } else {
        respond.respondNotFound(res, "Lesson-tag not exists");
      }
    })
   
    .catch((err) => {
      res.status(500).json({
        status: 0,
        message:
          Object.keys(err).length === 0
            ? "This lesson tag does not exist"
            : err,
      });
    });
}

const lessonTag_crud = {
  index,
  store,
  show,
  update,
  destroy,
};

module.exports = lessonTag_crud;
