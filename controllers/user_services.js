const respond = require("./api_controller");
const { userCreate } = require("../validators/validators");
const JWT = require("jsonwebtoken");
const fs = require("fs");
var multer = require("multer");
const modUser = require("../models/user");
const modCategories = require("../models/category");
const modLesson = require("../models/lesson");
const modLessonsFavorite = require("../models/lessons_favorite");
const modLessonComents = require("../models/lessons_comment");
const modLessonRating = require("../models/lessons_rating");
const modCommetLikes=require("../models/comments_like")
require("dotenv").config();
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "./public/uploads/profile");
  },
  filename: function (req, file, cb) {
    let random_val = Math.floor(Math.random() * 1000000);
    cb(null, random_val + file.originalname);
  },
});

function getAuthUser(req) {
  let userToken = req.headers["authorization"];
  return JWT.verify(userToken, process.env.JWT_SECRET, (error, decoded) => {
    if (decoded) {
      return modUser
        .findOne({
          where: {
            id: decoded.id,
          },
        })
        .then((user) => {
          if (user) {
            return user;
          } else {
            return false;
          }
        });
    } else {
      return false;
    }
  });
}

function updateProfile(req, res) {
  let upload = multer({
    storage: storage,
    limits: { fileSize: 1024 * 1024 * 1024 },
  }).single("profile_img");
  upload(req, res, function (err) {
    if (err) {
      res.status(404).json({
        status: 0,
        message: err,
      });
    }
    getAuthUser(req).then((authUser) => {
      if (!authUser) {
        respond.respondError(res, "Token is not valid!");
        return;
      }
      let path = req.file ? req.file.path : null;
      let old_path = authUser.img_url;
      let new_user = {
        ...authUser.dataValues,
        ...req.body,
        ...{ img_url: path ? path : old_path },
      };
      let validatorRes = userCreate(new_user);
      if (!validatorRes.is_valid) {
        respond.respondError(res, validatorRes.errors);
        return;
      }
      authUser
        .update(new_user)
        .then((updated_user) => {
          if (updated_user) {
            if (path) {
              try {
                fs.unlink(old_path, (err) => {});
              } catch {}
            }
            let _updatedUser = {
              id: updated_user.id,
              name: updated_user.name,
              email: updated_user.email,
              phone: updated_user.phone,
              img_url: updated_user.img_url,
              language: updated_user.language,
              membership: updated_user.membership,
            };
            respond.respondSuccess(res, _updatedUser);
          }
        })
        .catch((err) => {
          respond.respondError(res, err);
        });
    });
  });
}

function getProfile(req, res) {
  getAuthUser(req).then((user) => {
    if (user) {
      let _user = {
        id: user.id,
        name: user.name,
        email: user.email,
        phone: user.phone,
        img_url: user.img_url,
        language: user.language,
        membership: user.membership,
      };
      respond.respondSuccess(res, _user);
    } else {
      respond.respondNotFound(res, "User not found!");
    }
  });
}

function getVideoCategories(req, res) {
  modCategories.findAll().then((categories) => {
    if (categories.length > 0) {
      respond.respondSuccess(res, categories);
    } else {
      respond.respondNotFound(res, "Categories not exists");
    }
  });
}

function getVideoFavourites(req, res) {
  getAuthUser(req)
    .then((user) => {
      modLessonsFavorite
        .findAll({
          where: {
            userId: user.id,
          },
          attributes: ["id"],
          include: [{ model: modLesson, where: { type: 1 } }],
        })
        .then((data) => {
          respond.respondSuccess(res, data.test.test);
        });
    })
    .catch((err) => {
      respond.respondError(res, err, 500);
    });
}

function getVideosFromCategory(req, res) {
  modCategories
    .findOne({
      where: {
        id: req.params.id,
      },
      attributes: ["id", "name"],
      include: [{ model: modLesson, where: { type: 1 } }],
    })
    .then((category) => {
      if (category) {
        respond.respondSuccess(res, category);
      } else {
        respond.respondNotFound(res, "This category does not exists!");
      }
    });
}

async function getVideo(req, res) {
  let v_id = req.params.id;
  let authUser = await getAuthUser(req);

  let lesson = await modLesson.findOne({
    where: {
      id: v_id,
    },
    include: [
      { model: modLessonRating },
      { model: modLessonsFavorite },
      { model: modLessonComents,attributes:['comment','createdAt'],include:[{model:modUser,attributes:['id','name','img_url']}] },
    ],
  });

  

  if (!lesson) {
    respond.respondNotFound(res, "Video not found");
  }
  let comments= await lesson.lessons_comments.map(item=>{
    return  modCommetLikes.findAll().then(commentL=>{
      console.log("modCommetLikes .... :",modCommetLikes)
      item.likes=commentL.length
      let isLiked=commentL._find(c_l=>{
        return c_l.userId == authUser.id
      })
      item.isLiked=!!isLiked
    })

  })
  let rating_sum = 0;
  let is_favourite = lesson.lessons_favorites.length>0 ? true : false;
  lesson.lessons_ratings.forEach((item) => {
    rating_sum += parseInt(item.rating);
  });
  let rating =
    rating_sum == 0 ? 0 : rating_sum / lesson.lessons_favorites.length;
  let result ={
    id:lesson.id,
    url:lesson.url,
    title:lesson.title,
    description:lesson.description,
    categoryId:lesson.categoryId,
    rating:rating,
    isFavourite:is_favourite,
    comments:comments
  }
  respond.respondSuccess(res, result);
}
function getAudioCategories(req, res) {
  modCategories.findAll().then((categories) => {
    if (categories.length > 0) {
      respond.respondSuccess(res, categories);
    } else {
      respond.respondNotFound(res, "Categories not exists");
    }
  });
}

function getAudioFavourites(req, res) {
  getAuthUser(req)
    .then((user) => {
      modLessonsFavorite
        .findAll({
          where: {
            userId: user.id,
          },
          attributes: ["id"],
          include: [{ model: modLesson, where: { type: 0 } }],
        })
        .then((data) => {
          respond.respondSuccess(res, data.test.test);
        });
    })
    .catch((err) => {
      respond.respondError(res, err, 500);
    });
}

function getAudioFromCategory(req, res) {
  modCategories
    .findOne({
      where: {
        id: req.params.id,
      },
      attributes: ["id", "name"],
      include: [{ model: modLesson, where: { type: 0 } }],
    })
    .then((category) => {
      if (category) {
        respond.respondSuccess(res, category);
      } else {
        respond.respondNotFound(res, "This category does not exists!");
      }
    });
}

function getVideoComments(req, res) {
  const videoId = req.params.id;
  modLessonComents
    .findAll({ where: { lessonId: videoId } })
    .then((comments) => {
      if (comments.length > 0) {
        respond.respondSuccess(res, comments);
      } else {
        respond.respondNotFound(res, "No comments found for this video");
      }
    })
    .catch((err) => {
      respond.respondError(res, err, 500);
    });
}

async function storeVideoComments(req, res) {
  const videoId = req.params.id;
  let authUser = await getAuthUser(req);

  if (!authUser) {
    respond.respondNotFound(res, "User not authenticated");
    return;
  }

  let video = await modLesson.findOne({ where: { id: videoId } });

  if (!video) {
    respond.respondNotFound(res, "Video not found");
    return;
  }

  modLessonComents
    .create(Object.assign(req.body, { lessonId: videoId, userId: authUser.id }))
    .then((comment) => {
      respond.respondSuccess(res, comment);
    })
    .catch((err) => {
      respond.respondError(res, err, 500);
    });
}

async function destroyVideoComments(req, res) {
  let commentId = req.params.id;
  let authUser = await getAuthUser(req);

  if (!authUser) {
    respond.respondNotFound(res, "User not authenticated");
    return;
  }

  let comment = await modLessonComents.findOne({ where: { id: commentId } });

  if (!comment) {
    respond.respondNotFound(res, "Comment not found");
    return;
  } else {
    if (comment.userId === authUser.id) {
      modLessonComents
        .destroy({ where: { id: commentId } })
        .then(() => {
          respond.respondSuccess(res, "Comment successfully deleted");
        })
        .catch((err) => {
          respond.respondError(res, err, 500);
        });
    } else {
      respond.respondError(
        res,
        "You don't have ownership over this comment",
        422
      );
    }
  }
}

async function storeCommentsLike(req, res) {
  const commentId = req.params.id;

  let authUser = await getAuthUser(req);

  if (!authUser) {
    respond.respondNotFound(res, "User not authenticated");
    return;
  }

  const comment = await modCommentsLike.findOne({
    where: { lessonsCommentId: commentId, userId: authUser.id },
  });

  if (comment) {
    respond.respondNotFound(
      res,
      "You cannot give more than a like to a comment"
    );
    return;
  } else {
    modCommentsLike
      .create({
        lessonsCommentId: commentId,
        userId: authUser.id,
      })
      .then((like) => {
        respond.respondSuccess(res, like);
      })
      .catch((err) => {
        respond.respondError(res, err, 500);
      });
  }
}

let userServFunc = {
  updateProfile,
  getProfile,
  getVideoCategories,
  getVideoFavourites,
  getVideosFromCategory,
  getVideo,
  getAudioCategories,
  storeCommentsLike,
  destroyVideoComments,
  getVideoComments,
  storeVideoComments,
  getAudioFromCategory,
  getAudioFavourites

};
module.exports = userServFunc;
