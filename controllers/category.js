const validators = require("../validators/validators");
const modCategory = require("../models/category");
const respond = require("./api_controller");

async function index(req, res) {
  await modCategory
    .findAll()
    .then((items) => {
      if (items.length > 0) {
        respond.respondSuccess(res, items);
      } else {
        respond.respondNotFound(res, "Categories not exists");
      }
    })
    .catch((err) => {
      res.status(500).json({
        status: 0,
        errors: err,
      });
    });
}

function store(req, res) {
  let validatorRes = validators.categoryCreate(req.body);
  if (!validatorRes.is_valid) {
    respond.respondError(res, validatorRes.errors);
    return;
  }
  let newCategory = {
    name: req.body.name,
  };
  modCategory
    .create(newCategory)
    .then((category) => {
      if (category) {
        respond.respondSuccess(res, category);
      } else {
        respond.respondError(res, "Category registration failed");
      }
    })
    .catch((err) => {
      res.status(500).json({
        status: 0,
        message: err,
      });
    });
}

function show(req, res) {
  const categoryId = req.params.id;
  modCategory
    .findOne({
      where: {
        id: categoryId,
      },
    })
    .then((item) => {
      if (item) {
        respond.respondSuccess(res, item);
      } else {
        respond.respondNotFound(res, "This category does not exists");
      }
    })
    .catch((err) => {
      res.status(500).json({
        status: 0,
        message: err,
      });
    });
}

function update(req, res) {
  const categoryId = req.params.id;
  modCategory
    .findOne({
      where: {
        id: categoryId,
      },
    })
    .then((item) => {
      if (!item) {
        respond.respondNotFound(res, "This category does not exist");
      }
      let currCategory = item.dataValues;
      let data = { ...currCategory, ...req.body };
      let validatorRes = validators.categoryCreate(data);
      if (!validatorRes.is_valid) {
        respond.respondError(res, validatorRes.errors);
        return;
      }
      item.update(data).then(updatedCategory=>{
        respond.respondSuccess(res, updatedCategory);
      });
    })
    .catch((err) => {
      res.status(500).json({
        status: 0,
        errors:err,
      });
    });
}

function destroy(req, res) {
  const categoryId = req.params.id;
  modCategory
    .findOne({
      where: {
        id: categoryId,
      },
    })
    .then((item) => {
      if (item) {
        item
          .destroy()
          .then(respond.respondSuccess(res, "The category has been deleted"));
      } else {
        respond.respondNotFound(res, "Category not exists");
      }
    })
    .catch((err) => {
      res.status(500).json({
        status: 0,
        message:
          Object.keys(err).length === 0 ? "This category does not exist" : err,
      });
    }); 
}

const category_crud = {
  index,
  store,
  show,
  update,
  destroy,
};

module.exports = category_crud;
