const modUser = require("../models/user");
const modLessonFav = require("../models/lessons_favorite");
const { validate } = require("../db/db_connection");
require("dotenv").config();
const validators = require("../validators/validators");
const respond = require("./api_controller");
async function index(req, res) {
  let users = await modUser
    .findAll({
      include: [{ model: modLessonFav }],
    })
    .then((users) => {
      return users;
    });
  res.send(users);
}


function show(req, res) {
  const id = req.params.id;
  modUser
    .findOne({
      where: {
        id: id,
      },
    })
    .then((user) => {
      if (user) {
        respond.respondSuccess(res,user)
      } else {
        respond.respondNotFound(res)
      }
    })
    .catch((err) => {
      res.status(500).json({
        status: 0,
        data: err,
      });
    });
}

function update(req, res) {
   
}

function destroy(req, res) {
  const id = req.params.id;
  modUser
    .findOne({
      where: {
        id,
      },
    })
    .then((item) => {
      if(item){
        return item.destroy();
      }else{
        res.status(200).json({
          status: 0,
          data: "Invalid user id",
        });
      }
    })
    .then(result => {
      res.status(200).json({
        status: 1,
        data: result
      });
    })
    .catch((err) => {
      res.status(500).json({
        status: 0,
        message:err
      });
    });
}


const user_crud = {
  index,
  show,
  update,
  destroy,
};

module.exports = user_crud;
