const modAd = require("../models/ad");
const validators = require("../validators/validators");
const fs = require("fs");
const respond = require("./api_controller");
var multer = require("multer");
const { respondNotFound, respondSuccess } = require("./api_controller");
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "./public/uploads/ads");
  },
  filename: function (req, file, cb) {
    let random_val = Math.floor(Math.random() * 1000000);
    cb(null, random_val + file.originalname);
  },
});

async function index(req, res) {
  await modAd
    .findAll()
    .then((items) => {
      if (items.length > 0) {
        respond.respondSuccess(res, items);
      } else {
        respond.respondNotFound(res, "Not any ads exists");
      }
    })
    .catch((err) => {
      res.status(500).json({
        status: 0,
        errors: err,
      });
    });
}

function store(req, res) {
  let upload = multer({
    storage: storage,
    limits: { fileSize: 10240 * 10240 * 10240 },
  }).single("ad_file");
  upload(req, res, function (err) {
    if (err) {
      respond.respondNotFound(res, "Upload failed");
    } else {
      let path = req.file ? req.file.path : null;
      if (!path) {
        respond.respondNotFound(res, "Invalid input");
        return;
      }
      let newAdd = {
        url: path,
      };
      modAd
        .create(newAdd)
        .then((ad) => {
          respond.respondSuccess(res, ad);
        })
        .catch((err) => {
          respond.respondError(res, err);
        });
    }
  });
}

function show(req, res) {
  modAd
    .findOne({
      where: {
        id: req.params.id,
      },
    })
    .then((ad) => {
      if (ad) {
        respond.respondSuccess(res, ad);
      } else {
        respond.respondNotFound(res, "Ad not found");
      }
    });
}

function update(req, res) {
  let upload = multer({
    storage: storage,
    limits: { fileSize: 1024 * 1024 * 1024 },
  }).single("ad_file");
  upload(req, res, function (err) {
    let path = req.file ? req.file.path : null;
    if (err) {
      respond.respondNotFound(res, err);
    }
    modAd
      .findOne({
        where: {
          id: req.params.id,
        },
      })
      .then((ad) => {
        if (!ad) {
          respond.respondError(res, "Invalid ad id!");
        }
        if (path) {
          fs.unlink(ad.url, (err) => {});
          ad.update({
            url: path,
          }).then((new_ad) => {
            respondSuccess(res, new_ad);
          });
        } else {
          respond.respondNotFound(res, "File missing!");
        }
      });
  });
}

function destroy(req, res) {
  const id = req.params.id;
  modAd
    .findOne({
      where: {
        id: id,
      },
    })
    .then((item) => {
      if (item) {
        fs.unlink(item.url, (err) => {});
        item
          .destroy()
          .then(respond.respondSuccess(res, "Ad has been deleted"));
      } else {
        respond.respondNotFound(res, "Ad not exists");
      }
    })
    .catch((err) => {
      res.status(400).json({
        status: 0,
        message: Object.keys(err).length === 0 ? "This ad does not exist" : err,
      });
    });
}

const ad_crud = {
  index,
  store,
  show,
  update,
  destroy,
};

module.exports = ad_crud;
