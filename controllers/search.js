const modLessonTag = require("../models/lessons_tag");
const modLesson = require("../models/lesson");
const modCategory = require("../models/category");

function search(req, res) {
  // const category = req.query.category.split(",");
  if (typeof req.query.tag === "undefined") {
    const category = req.query.category.split(",");
    modCategory
      .findAll({ where: { id: category } })
      .then((category) => {
        let categoryId = [];
        category.forEach((item) => categoryId.push(item.id));
        return categoryId;
      })
      .then((lesson) => {
        let video = [];
        let audio = [];

        modLesson.findAll({ where: { categoryId: lesson } }).then((result) => {
          result.forEach((item) => {
            if (item.dataValues.type) {
              video.push(item);
            } else {
              audio.push(item);
            }
          });

          res.status(200).json({
            status: 0,
            video,
            audio,
          });
        });
      })
      .catch((err) =>
        res.status(400).json({
          status: 0,
          errors: err,
        })
      );
  } else if (typeof req.query.category == "undefined") {
    const tags = req.query.tag.split(",");
    modLessonTag
      .findAll({ where: { tagId: tags } })
      .then((lessons) => {
        let lessonId = [];
        lessons.forEach((item) => lessonId.push(item.lessonId));
        return lessonId;
      })
      .then((lesson) => {
        let video = [];
        let audio = [];

        modLesson.findAll({ where: { id: lesson } }).then((result) => {
          result.forEach((item) => {
            if (item.dataValues.type) {
              video.push(item);
            } else {
              audio.push(item);
            }
          });

          res.status(200).json({
            status: 0,
            video,
            audio,
          });
        });
      })
      .catch((err) =>
        res.status(400).json({
          status: 0,
          errors: err,
        })
      );
  } else {
    const category = req.query.category.split(",");
    const tags = req.query.tag.split(",");
    modLessonTag
      .findAll({ where: { tagId: tags } })
      .then((lessons) => {
        let lessonId = [];
        lessons.forEach((item) => lessonId.push(item.lessonId));
        return lessonId;
      })
      .then((lesson) => {
        modLesson
          .findAll({ where: { id: lesson, categoryId: [...category] } })
          .then((result) => {
            let audio = [];
            let video = [];

            result.forEach((item) => {
              if (item.dataValues.type) {
                video.push(item);
              } else {
                audio.push(item);
              }
            });

            res.status(200).json({
              status: 0,
              video,
              audio,
            });
          });
      })
      .catch((err) =>
        res.status(400).json({
          status: 0,
          errors: err,
        })
      );
  }
}

const searchQuery = {
  search,
};

module.exports = searchQuery;
