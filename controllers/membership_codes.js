const modMembershipCodes = require("../models/memberships_code");
const generator_code = require("./../constants/code_generator");

function index(req, res) {}

function store(req, res) {
  const code = generator_code(8);
  modMembershipCodes
    .findOne({
      where: {
        code: code,
      },
    })
    .then((item) => {
      if (item) {
        res.status(422).json({
          status: 0,
          message: "This code already exists!",
        });
      } else {
        modMembershipCodes
          .create({
            code,
          })
          .then((newItem) => {
            res.status(200).json({
              status: 1,
              result: newItem,
            });
          })
          .catch(err => {
              res.status(500).json({
                status: 0,
                message: err,
              });
          })
      }
    })
    .catch((err) => {
      res.status(500).json({
        status: 0,
        message: err,
      });
    });
}

function show(req, res) {}

function update(req, res) {}

function destroy(req, res) {}

const membershipCodes_crud = {
  index,
  store,
  show,
  update,
  destroy,
};

module.exports = membershipCodes_crud;
