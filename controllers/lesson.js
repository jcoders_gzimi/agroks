const modLesson = require("../models/lesson");
const modCategory = require("../models/category");
const validators = require("../validators/validators");
const fs = require("fs");
const respond=require("./api_controller")
var multer = require("multer");
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "./public/uploads/lessons");
  },
  filename: function (req, file, cb) {
    let random_val = Math.floor(Math.random() * 1000000);
    cb(null, random_val + file.originalname);
  },
});
async function index(req, res) {
  await modLesson
    .findAll()
    .then((items) => {
      res.status(200).json({
        status: 0,
        message: items,
      });
    })
    .catch((err) => {
      res.status(400).json({
        status: 0,
        errors: err,
      });
    });
}

function store(req, res) {
  let upload = multer({
    storage: storage,
    limits: { fileSize: 1024 * 1024 * 1024 },
  }).single("lesson_file");
  upload(req, res, function (err) {
    if (err) {
      res.status(404).json({
        status: 0,
        message: "File not found!",
      });
    } else {
      let path = req.file ? req.file.path : null;
      if (!path) {
        res.status(404).json({
          status: 0,
          message: "File not found!",
        });
        return;
      }
      let validatorRes = validators.lessonCreate(req.body);
      if (!validatorRes.is_valid) {
        fs.unlink(req.file.path, (err) => {});
        respond.respondError(res, validatorRes.errors);
        return;
      }
      let newLesson = {
        url: path,
        title: req.body.title,
        description: req.body.description,
        categoryId: req.body.categoryId,
        type:req.body.type
      };
      modCategory
        .findOne({
          where: {
            id: newLesson.categoryId,
          },
        })
        .then((item) => {
          if (item) {
            modLesson
              .create(newLesson)
              .then((lesson) => {
                res.status(200).json({
                  status: 1,
                  message: lesson,
                });
              })
              .catch((err) => {
                res.status(400).json({
                  status: 0,
                  message: err,
                });
              });
          } else {
            res.status(404).json({
              status: 0,
              message: "This category does not exist",
            });
          }
        })
        .catch((err) => {
          res.status(422).json({
            status: 0,
            message: err,
          });
        });
    }
  });
}

function show(req, res) {
  const lessonId = req.params.id;
  modLesson
    .findOne({
      where: {
        id: lessonId,
      },
    })
    .then((lesson) => {
      if (lesson) {
        respond.respondSuccess(res, lesson);
      } else {
        respond.respondNotFound(res, "Lesson not found");
      }
    });
}

function update(req, res) {
  let upload = multer({
    storage: storage,
    limits: { fileSize: 1024 * 1024 * 1024 },
  }).single("lesson_file");
  upload(req, res, function (err) {
    console.log(req.file);
    let path = req.file ? req.file.path : null;

    if (err) {
      res.status(404).json({
        status: 0,
        message: err,
      });
    }
    modLesson
      .findOne({
        where: {
          id: req.params.id,
        },
      })
      .then((lesson) => {
        if (!lesson) {
          return res.status(422).json({
            status: 0,
            message: "Invalid lesson id!",
          });
        }
        if (path) {
          let currLesson = lesson.dataValues;
          let data = { ...currLesson, ...req.body, ...{ url: path } };
          let validatorRes = validators.lessonCreate(data);
          if (!validatorRes.is_valid) {
            fs.unlink(req.file.path, (err) => {});
            res.status(422).json({
              status: 0,
              errors: validatorRes.errors,
            });
            return;
          }
          fs.unlink(lesson.url, (err) => {});
          lesson
            .update(data)
            .then((new_lesson) => {
              return res.status(201).json({
                status: 1,
                data: new_lesson,
              });
            })
            .catch((err) => {});
        } else {
          let currLesson = lesson.dataValues;
          let data = { ...currLesson, ...req.body };
          let validatorRes = validators.lessonCreate(data);
          if (!validatorRes.is_valid) {
            res.status(422).json({
              status: 0,
              errors: validatorRes.errors,
            });
            return;
          }
          lesson.update(data).then((new_lesson) => {
            return res.status(201).json({
              status: 1,
              data: new_lesson,
            });
          });
        }
      });
  });
}

function destroy(req, res) {
  const lessonId = req.params.id;
  modLesson
    .findOne({
      where: {
        id: lessonId,
      },
    })
    .then((item) => {
      fs.unlink(item.url, (err) => {});
      return item.destroy();
    })
    .then(() => {
      res.status(200).json({
        status: 1,
        message: "The lesson has been deleted",
      });
    })
    .catch((err) => {
      res.status(400).json({
        status: 0,
        message:
          Object.keys(err).length === 0 ? "This lesson does not exist" : err,
      });
    });
}

const lesson_crud = {
  index,
  store,
  show,
  update,
  destroy,
};

module.exports = lesson_crud;
