const modUser = require("../models/user");
const bcrypt = require("bcrypt");
const User = require("../models/user");
const JWT = require("jsonwebtoken");
require("dotenv").config();
const validators = require("../validators/validators");
const respond = require("./api_controller");
const nodemailer = require("nodemailer");
const generator = require("./../constants/code_generator");
const code_generator = require("./../constants/code_generator");

function register(req, res) {
  let validatorRes = validators.userCreate(req.body);
  if (!validatorRes.is_valid) {
    respond.respondError(res, validatorRes.errors);
    return;
  }

  let newUser = {
    name: req.body.name,
    email: req.body.email,
    password: bcrypt.hashSync(req.body.password, 10),
    phone: req.body.phone,
  };

  modUser
    .findOne({
      where: {
        email: newUser.email,
      },
    })
    .then((user) => {
      if (user) {
        respond.respondError(res, "User already exists!");
      } else {
        modUser
          .create(newUser)
          .then((createdUser) => {
            login(req, res);
            return;
          })
          .catch((err) => {
            res.status(500).json({
              status: 0,
              error: err,
            });
          });
      }
    });
}

function login(req, res) {
  User.findOne({
    where: {
      email: req.body.email,
    },
  })
    .then((user) => {
      if (user) {
        if (
          bcrypt.compareSync(
            req.body.password ? req.body.password : "",
            user.password
          )
        ) {
          let userToken = JWT.sign(
            {
              email: user.email,
              id: user.id,
            },
            process.env.JWT_SECRET
          );

          res.status(200).json({
            status: 1,
            message: "User Logged In Successfully",
            token: userToken,
            user: {
              id: user.id,
              name: user.name,
              phone: user.phone,
              img_url: user.img_url,
              language: user.language,
              is_admin: user.is_admin,
              is_superadmin: user.is_superadmin,
              membership: user.membership,
            },
          });
        } else {
          res.status(500).json({
            status: 0,
            message: "Password didn't match",
          });
        }
      } else {
        res.status(500).json({
          status: 0,
          message: "User not exists with this email address",
        });
      }
    })
    .catch((err) => {
      console.log(err);
    });
}

function changePassword(req, res) {
  let userToken = req.headers["authorization"];
  if (!req.body.newPwd || !req.body.curPwd || !req.body.password) {
    res.status(400).json({
      status: 0,
      message: "Required fields should be filled",
    });
    return;
  }

  if (req.body.newPwd.length <= 6) {
    res.status(400).json({
      status: 0,
      message: "Password is too short",
    });
    return;
  }

  if (req.body.newPwd != req.body.confirmPwd) {
    res.status(400).json({
      status: 0,
      message: "Passwords do not match",
    });
    return;
  }

  if (userToken) {
    JWT.verify(userToken, process.env.JWT_SECRET, (error, decoded) => {
      if (decoded) {
        User.findOne({
          where: {
            id: decoded.id,
          },
        }).then((user) => {
          if (
            bcrypt.compareSync(
              req.body.password ? req.body.password : "",
              user.password
            )
          ) {
            newPass = bcrypt.hashSync(req.body.newPwd, 10);
            user.update({ password: newPass }).then((result) => {
              res.status(200).json({
                status: 1,
                message: "Password changed successfully",
              });
            });
          } else {
            res.status(500).json({
              status: 0,
              message: "Your old password do not match",
            });
          }
        });
      } else {
        res.status(500).json({
          status: 0,
          message: "Token is not valid",
        });
      }
    });
  } else {
    res.status(500).json({
      status: 0,
      message: "Please provide authentication token value",
    });
  }
}

async function sendEmailReset(req, res) {
  let userToken;

  await User.findOne({
    where: { email: req.body.email },
  }).then((user) => {
    if (user) {
      userToken = JWT.sign(
        {
          email: user.email,
          id: user.id,
        },
        process.env.JWT_RESET,
        {
          expiresIn: "10m",
        }
      );
    } else {
      res.status(404).json({
        status: 0,
        message: "This user doesn't exist",
      });
    }
  });

  let generatedPwd = code_generator(8);

  let transporter = nodemailer.createTransport({
    service: "gmail",
    auth: {
      user: "n.katona13@gmail.com",
      pass: "xhaspcsrsspefcnq",
    },
  });

  let mailOptions = {
    from: "n.katona13@gmail.com",
    to: "shkumbinbajrami91@gmail.com",
    subject: "Testing and Testing",
    html:
      `Passwordi juaj i ri eshte: <h1>${generatedPwd}</h1>, kliko linkun per te aktivizuar passwordin e ri.
    Nese nuk e keni bere kete kerkese ju lutem injorojeni kete email!<br>` +
      `http://localhost:3000/api/forgotPassword/${userToken}?newPwd=${generatedPwd}`.replace(
        /\s/g,
        ""
      ),
  };

  transporter.sendMail(mailOptions, function (err, data) {
    if (err) {
      res.status(404).json({
        status: 0,
        message: "Email failed !",
      });
    } else {
      res.status(200).json({
        status: 1,
        message: "Email sent !",
      });
    }
  });
}

async function confirmEmailReset(req, res) {
  const userToken = req.params.token;
  const newPwd = req.query.newPwd;

  if (userToken) {
    await JWT.verify(userToken, process.env.JWT_RESET, (error, decoded) => {
      if (decoded) {
        User.findOne({
          where: {
            id: decoded.id,
          },
        })
          .then((user) => {
            let newPass = bcrypt.hashSync(newPwd, 10);
            user.update({ password: newPass }).then((result) => {
              res.status(200).json({
                status: 1,
                message: "Password changed successfully",
              });
            });
          })
          .catch((err) => {
            res.status(500).json({
              status: 0,
              message: "Password not changed",
            });
          });
      } else {
        res.status(500).json({
          status: 0,
          message: "Token is not valid",
        });
      }
    });
  } else {
    res.status(500).json({
      status: 0,
      message: "Please provide authentication token value",
    });
  }
}

const auth = {
  register,
  login,
  changePassword,
  sendEmailReset,
  confirmEmailReset,
};

module.exports = auth;
