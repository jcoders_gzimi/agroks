var express = require("express");
var router = express.Router();
const cntAd = require("../controllers/ad");
const mdlIsAdmin = require("../middlewares/mdlIsAdmin");


router.get("/", mdlIsAdmin, function (req, res, next) {
  cntAd.index(req, res);
});
router.get("/:id", mdlIsAdmin, function (req, res, next) {
  cntAd.show(req, res);
});
router.post("/", mdlIsAdmin, function (req, res, next) {
  cntAd.store(req, res);
});
router.put("/:id", mdlIsAdmin, function (req, res, next) {
  cntAd.update(req, res);
});
router.delete("/:id", mdlIsAdmin, function (req, res, next) {
  cntAd.destroy(req, res);
});

module.exports = router;
