var express = require("express");
var router = express.Router();
const cntUser = require("../controllers/user");
const mdlIsLoggedIn = require("../middlewares/mdlIsLoggedIn");
const mdlIsAdmin = require("../middlewares/mdlIsAdmin");
/* GET users listing. */
router.get("/", mdlIsLoggedIn, function (req, res, next) {
  cntUser.index(req, res);
});
router.get("/:id", mdlIsAdmin, function (req, res, next) {
  cntUser.show(req, res);
});


router.put("/:id", function (req, res, next) {
  res.send("put " + req.params.id);
});

router.delete("/:id", function (req, res, next) {
    cntUser.destroy(req, res);
});


router.get("/users/:id/lesson_favourites");

module.exports = router;
