var express = require("express");
var router = express.Router();
const cntLesson = require("../controllers/lesson");
const mdlIsAdmin = require("../middlewares/mdlIsAdmin");

/* GET users listing. */
router.get("/", mdlIsAdmin, function (req, res, next) {
  cntLesson.index(req, res);
});
router.get("/:id", mdlIsAdmin, function (req, res, next) {
  cntLesson.show(req, res);
});
router.post("/", mdlIsAdmin, function (req, res, next) {
  cntLesson.store(req, res);
});
router.put("/:id", mdlIsAdmin, function (req, res, next) {
  cntLesson.update(req, res);
});
router.delete("/:id", mdlIsAdmin, function (req, res, next) {
  cntLesson.destroy(req, res);
});

module.exports = router;
