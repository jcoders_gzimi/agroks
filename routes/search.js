var express = require("express");
var router = express.Router();
const cntSearch = require("../controllers/search");

// GET by Tag
router.get("/?", function (req, res, next) {
  cntSearch.search(req, res);
});

module.exports = router;
