var express = require("express");
var router = express.Router();
const mdlIsLoggedIn = require("../middlewares/mdlIsLoggedIn");
const modUserServices=require("../controllers/user_services")
// profile
router.get("/profile", function (req, res, next) {
  modUserServices.getProfile(req, res);
});
router.put("/profile",mdlIsLoggedIn, function (req, res, next) {
  modUserServices.updateProfile(req, res);
});
//

// lesson categories
router.get("/video-categories", function (req, res, next) {
  modUserServices.getVideoCategories(req, res);
});
router.get("/video-favorites", function (req, res, next) {
  modUserServices.getVideoFavourites(req, res);
});

router.get("/video-categories/:id", function (req, res, next) {
  modUserServices.getVideosFromCategory(req, res);
});

router.get("/video/:id", function (req, res, next) {
  modUserServices.getVideo(req, res);
});
// router.get("/lessonCategories/:id", function (req, res, next) {
//     cntUser.index(req, res);
//   });
//


module.exports = router;
