var express = require("express");
var router = express.Router();
const cntCategory = require("../controllers/category");
const mdlIsAdmin = require("../middlewares/mdlIsAdmin");

/* GET users listing. */
router.get("/", mdlIsAdmin, function (req, res, next) {
  cntCategory.index(req, res);
});
router.get("/:id", mdlIsAdmin, function (req, res, next) {
  cntCategory.show(req, res);
});
router.post("/", mdlIsAdmin, function (req, res, next) {
  cntCategory.store(req, res);
});
router.put("/:id", mdlIsAdmin, function (req, res, next) {
  cntCategory.update(req, res);
});
router.delete("/:id", mdlIsAdmin, function (req, res, next) {
  cntCategory.destroy(req, res);
});

module.exports = router;
