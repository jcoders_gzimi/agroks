var express = require("express");
var router = express.Router();
const cntTag = require("../controllers/tag");
/* GET users listing. */
router.get("/", function (req, res, next) {
  cntTag.index(req, res);
});
router.get("/:id", function (req, res, next) {
  cntTag.show(req, res);
});
router.post("/", function (req, res, next) {
  cntTag.store(req, res);
});
router.put("/:id", function (req, res, next) {
  cntTag.update(req, res);
});
router.delete("/:id", function (req, res, next) {
  cntTag.destroy(req, res);
});

module.exports = router;
