var express = require("express");
var router = express.Router();
const cntLessonTag = require("../controllers/lessons_tag");
const mdlIsAdmin = require("../middlewares/mdlIsAdmin");

/* GET users listing. */
router.get("/", mdlIsAdmin, function (req, res, next) {
  cntLessonTag.index(req, res);
});
router.get("/:id", mdlIsAdmin, function (req, res, next) {
  cntLessonTag.show(req, res);
});
router.post("/", mdlIsAdmin, function (req, res, next) {
  cntLessonTag.store(req, res);
});
router.put("/:id", mdlIsAdmin, function (req, res, next) {
  cntLessonTag.update(req, res);
});
router.delete("/:id", mdlIsAdmin, function (req, res, next) {
  cntLessonTag.destroy(req, res);
});

module.exports = router;
