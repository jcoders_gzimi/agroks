var express = require("express");
var router = express.Router();
const cntMembershipsCode=require('../controllers/membership_codes')
/* GET users listing. */
router.get("/", function (req, res, next) {
  res.send("respond with a resource");
});
router.get("/:id", function (req, res, next) {
  res.send("respond with a resource");
});
router.post("/", function (req, res, next) {
  cntMembershipsCode.store(req,res)
});
router.put("/:id", function (req, res, next) {
  res.send("respond with a resource");
});
router.delete("/:id", function (req, res, next) {
  res.send("respond with a resource");
});

module.exports = router;
