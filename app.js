var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
var multer = require("multer");

var adsRouter = require("./routes/ads");
var categoriesRouter = require("./routes/categories");
var lessonsRouter = require("./routes/lessons");
var memberShipCodesRouter = require("./routes/membership_codes");
var tagsRouter = require("./routes/tags");
var usersRouter = require("./routes/users");
var userRouter = require("./routes/user");
var lessonTagRouter = require("./routes/lessons_tag");
var searchRouter = require("./routes/search");

const Sequelize = require("sequelize");
const sequelize = require("./db/db_connection");
// "start": "node ./bin/www"

// import Models
const modUser = require("./models/user");
const modAd = require("./models/ad");
const modTag = require("./models/tag");
const modCategory = require("./models/category");
const modAdsTag = require("./models/ads_tag");
const modLesson = require("./models/lesson");
const modLessonsComment = require("./models/lessons_comment");
const modLessonsFavorite = require("./models/lessons_favorite");
const modLessonsRating = require("./models/lessons_rating");
const modLessonsTag = require("./models/lessons_tag");
const modMembershipsCode = require("./models/memberships_code");
const modMembership = require("./models/membership");
const modCommentsLike = require("./models/comments_like");
const cntAuth = require("./controllers/auth");
const mdlIsLoggedIn = require("./middlewares/mdlIsLoggedIn");

var app = express();

sequelize.sync();
// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use("/public", express.static(path.join(__dirname, "public")));
app.use("/api/admin/ads", adsRouter);
app.use("/api/search", searchRouter);
app.use("/api/admin/categories", categoriesRouter);
app.use("/api/admin/lessons", lessonsRouter);
app.use("/api/admin/membership_codes", memberShipCodesRouter);
app.use("/api/admin/tags", tagsRouter);
app.use("/api/admin/users", usersRouter);

app.use("/api/admin/lesson-tag", lessonTagRouter);
app.use("/api/user", userRouter);
app.post("/api/login", function (req, res) {
  cntAuth.login(req, res);
});
app.post("/api/signup", function (req, res) {
  cntAuth.register(req, res);
});
app.post("/api/resetPassword", mdlIsLoggedIn, function (req, res) {
  cntAuth.changePassword(req, res);
});
app.get("/api/forgotPassword", function (req, res) {
  cntAuth.sendEmailReset(req, res);
});

app.get("/api/forgotPassword/:token", function (req, res) {
  cntAuth.confirmEmailReset(req, res);
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
