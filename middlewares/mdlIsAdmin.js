const { response } = require("../app");
const JWT = require("jsonwebtoken");
const User = require("../models/user");
require("dotenv").config();

function isAdmin(req, res, next) {
  let userToken = req.headers["authorization"];

  if (userToken) {
    JWT.verify(userToken, process.env.JWT_SECRET, (error, decoded) => {
      if (decoded) {
        User.findOne({
          where: {
            id: decoded.id,
          },
        }).then((user) => {
          if (user.is_admin == 1) {
            next();
          } else {
            res.status(401).json({
              status: 0,
              message: "You're not authorized!",
            });
          }
        });
      } else {
        res.status(500).json({
          status: 0,
          message: "Token is not valid",
        });
      }
    });
  } else {
    res.status(500).json({
      status: 0,
      message: "Please provide authentication token value",
    });
  }
}

module.exports = isAdmin;
