const { response } = require("../app");
const JWT = require("jsonwebtoken");
require("dotenv").config();

function isLoggedIn(req, res, next) {
  let userToken = req.headers["authorization"];
  if (userToken) {
    JWT.verify(userToken, process.env.JWT_SECRET, (error, decoded) => {
      if (decoded) {
        next();
      } else {
        res.status(401).json({
          status: 0,
          message: "You're not logged in. Log in to continue !",
        });
      }
    });
  } else {
    res.status(500).json({
      status: 0,
      message: "Please provide authentication token value",
    });
  }
}

module.exports = isLoggedIn;
