const joi = require("@hapi/joi");
const v_messages = require("./messages");
let userCreateValidator = joi
  .object({
    name: joi.string().required().messages({
      "string.empty": v_messages.en.user_create.name,
      "any.required": v_messages.en.user_create.name,
    }),
    email: joi.string().email().required().messages({
      "string.empty": v_messages.en.user_create.email_empty,
      "string.email": v_messages.en.user_create.email_format,
      "any.required": v_messages.en.user_create.email_empty,
    }),
    password: joi.string().min(6).required().messages({
      "string.empty": v_messages.en.user_create.pass_empty,
      "string.min": v_messages.en.user_create.pass_length,
      "any.required": v_messages.en.user_create.pass_empty,
    }),
    phone: joi.string().required().messages({
      "string.empty": v_messages.en.user_create.phone,
      "any.required": v_messages.en.user_create.phone,
    }),
  })
  .options({ abortEarly: false, allowUnknown: true });

function getErrors(details) {
  let errors = details.map((item) => {
    let err = { [item.context.key]: item.message };
    return err;
  });
  return errors;
}
function userCreate(data) {
  let result = { is_valid: true, errors: {}, values: {} };
  let valid_res = userCreateValidator.validate(data);
  if (valid_res.error) {
    result.is_valid = false;
    result.errors = getErrors(valid_res.error.details);
  }
  result.values = valid_res.value;
  return result;
}
// Ad validator
let adCreateValidator = joi
  .object({
    url: joi.string().required().messages({
      "string.empty": v_messages.en.ad_create.url,
      "any.required": v_messages.en.ad_create.url,
    }),
  })
  .options({ abortEarly: false, allowUnknown: true });

function adCreate(data) {
  let result = { is_valid: true, errors: {}, values: {} };
  let valid_res = adCreateValidator.validate(data);
  if (valid_res.error) {
    result.is_valid = false;
    result.errors = getErrors(valid_res.error.details);
  }
  result.values = valid_res.value;
  return result;
}

// Tag validator//
let tagCreateValidator = joi
  .object({
    name: joi.string().required().messages({
      "string.empty": v_messages.en.tag_create.name,
      "any.required": v_messages.en.tag_create.name,
      "string.base": v_messages.en.tag_create.name_text,
    }),
  })
  .options({ abortEarly: false, allowUnknown: true });

function tagCreate(data) {
  let result = { is_valid: true, errors: {}, values: {} };
  let valid_res = tagCreateValidator.validate(data);
  if (valid_res.error) {
    result.is_valid = false;
    result.errors = getErrors(valid_res.error.details);
  }
  result.values = valid_res.value;
  return result;
}

//Lesson Tag validator
let lessonTagCreateValidator = joi
  .object({
    tagId: joi.string().required().messages({
      "string.empty": v_messages.en.lessonTag_create.tagId,
      "any.required": v_messages.en.lessonTag_create.tagId,
    }),
    lessonId: joi.string().required().messages({
      "string.empty": v_messages.en.lessonTag_create.lessonId,
      "any.required": v_messages.en.lessonTag_create.lessonId,
    }),
  })
  .options({ abortEarly: false, allowUnknown: true });

function lessonTagCreate(data) {
  let result = { is_valid: true, errors: {}, values: {} };
  let valid_res = lessonTagCreateValidator.validate(data);
  if (valid_res.error) {
    result.is_valid = false;
    result.errors = getErrors(valid_res.error.details);
  }
  result.values = valid_res.value;
  return result;
}

// Category validator
let categoryCreateValidator = joi
  .object({
    name: joi.string().required().messages({
      "string.empty": v_messages.en.category_create.name,
      "any.required": v_messages.en.category_create.name,
      "string.base": v_messages.en.category_create.name_text,
    }),
  })
  .options({ abortEarly: false, allowUnknown: true });

function categoryCreate(data) {
  let result = { is_valid: true, errors: {}, values: {} };
  let valid_res = categoryCreateValidator.validate(data);
  if (valid_res.error) {
    result.is_valid = false;
    result.errors = getErrors(valid_res.error.details);
  }
  result.values = valid_res.value;
  return result;
}
//Lesson validator
let lessonCreateValidator = joi
  .object({
    title: joi.string().required().messages({
      "string.empty": v_messages.en.lesson_create.title,
      "any.required": v_messages.en.lesson_create.title,
    }),
    description: joi.string().required().messages({
      "string.empty": v_messages.en.lesson_create.description,
      "any.required": v_messages.en.lesson_create.description,
    }),
    categoryId: joi.number().integer().required().messages({
      "string.empty": v_messages.en.lesson_create.categoryId_require,
      "any.required": v_messages.en.lesson_create.categoryId_require,
    }),
  })
  .options({ abortEarly: false, allowUnknown: true });

function lessonCreate(data) {
  let result = { is_valid: true, errors: {}, values: {} };
  let valid_res = lessonCreateValidator.validate(data);
  if (valid_res.error) {
    result.is_valid = false;
    result.errors = getErrors(valid_res.error.details);
  }
  result.values = valid_res.value;
  return result;
}

// Memberships validator
let membershipCreateValidator = joi
  .object({
    startDate: joi.date().iso().required().messages({
      "string.empty": v_messages.en.category_create.name,
      "any.required": v_messages.en.category_create.name,
      "string.base": v_messages.en.category_create.name_text,
    }),
  })
  .options({ abortEarly: false, allowUnknown: true });

function membershipCreate(data) {
  let result = { is_valid: true, errors: {}, values: {} };
  let valid_res = membershipCreateValidator.validate(data);
  if (valid_res.error) {
    result.is_valid = false;
    result.errors = getErrors(valid_res.error.details);
  }
  result.values = valid_res.value;
  return result;
}

const validators = {
  userCreate,
  adCreate,
  categoryCreate,
  lessonCreate,
  tagCreate,
  lessonTagCreate,
};
module.exports = validators;
