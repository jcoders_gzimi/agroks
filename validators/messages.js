const al = {
  user_create: {
    name: "Emri duhet plotesuar!",
    mail_empty: "Email duhet plotesuar!",
    email_format: "Email nuk eshte valid",
    pass_empty: "Fjalekalimi duhet plotesuar!",
    pass_length: "Fjalekalimi duhet te kete se paku 6 karaktere!",
    phone: "Numri i telefonit duhet plotesuar!",
  },
  ad_create: {
    url: "Url duhet plotesuar!",
  },
  adTag_create: {
    tagId: "Etiketa duhet plotesuar!",
    tagId_exist: "Kjo etikete nuk ekziston",
    adId: "Reklama duhet plotesuar!",
    adId_exist: "Kjo reklame nuk ekziston!",
  },
  category_create: {
    name_require: "Emri duhet plotesuar!",
    name_text: "Emri duhet te permbaje tekst!",
  },
  lesson_create: {
    url: "Url duhet plotesuar!",
    title: "Titulli duhet plotesuar!",
    description: "Pershkrimi duhet plotesuar!",
    categoryId_require: "Kategoria duhet plotesuar!",
    categoryId_exist: "Kjo kategori nuk ekziston!",
  },
  lessonsComments_create: {
    comment: "Komenti duhet plotesuar!",
  },
  lessonsRating_create: {
    rating: "Vleresimi duhet plotesuar!",
  },
  tag_create: {
    name: "Emri duhet plotesuar!",
    name_text: "Emri duhet te permbaje vetem shkronja!",
  },
  lessonTag_create: {
    tagId: "Etiketa duhet plotesuar!",
    tagId_exist: "Kjo etikete nuk ekziston!",
    lessonId: "Video ose audio duhet plotesuar!",
    lessonId_exist: "Video ose audio nuk ekziston!",
  },
  memberships_create: {
    startDate: "Data e fillimit duhet plotesuar!",
    endDate: "Data e perfundimit duhet plotesuar!",
    codeId: "Kodi duhet plotesuar!",
    codeId_exist: "Ky kod nuk ekziston!",
    date_format: "Formati i dates nuk eshte valid!",
    userId: "Useri duhet plotesuar!",
    userId_exist: "Ky user nuk ekziston!",
  },
  membershipsCodes_create: {
    code: "Kodi duhet plotesuar!",
    status: "Statusi duhet plotesuar!",
  },
};

const en = {
  user_create: {
    name: "Name is required!",
    email_empty: "Email is required!",
    email_format: "This email is not valid",
    pass_empty: "Password is required!",
    pass_length: "Password must contain at least 6 characters!",
    phone: "Phone is required!",
  },
  ad_create: {
    url: "Url is required!",
  },
  adTag_create: {
    tagId: "Tag is required",
    tagId_exist: "This tag does not exist!",
    adId: "Ad is required",
    adId_exist: "This ad does not exist!",
  },
  category_create: {
    name: "Name is required!",
    name_text: "Name must contain only letters!",
  },
  lessonsComments_create: {
    comment: "Comment is required!",
  },
  lessonsRating_create: {
    rating: "Rating is required!",
  },
  lesson_create: {
    url: "Url is required!",
    title: "Title is required!",
    description: "Description is required!",
    categoryId_require: "Description is required!",
    categoryId_exist: "This category does not exist!",
  },
  tag_create: {
    name: "Name is required!",
    name_text: "Name must contain only letters!",
  },
  lessonTag_create: {
    tagId: "Tag is required",
    tagId_exist: "This tag does not exist!",
    lessonId: "Lesson is required",
    lessonId_exist: "This lesson does not exist!",
  },
  memberships_create: {
    startDate: "Start date is required!",
    endDate: "End date is required!",
    codeId: "Code is required",
    codeId_exist: "This code does not exist!",
    date_format: "Date format is not valid!",
    userId: "User is required",
    userId_exist: "This user does not exist!",
  },
  membershipsCodes_create: {
    code: "Code is required!",
    status: "Status is required!",
  },
};

const validator_messages = {
  al,
  en,
};
module.exports = validator_messages;
